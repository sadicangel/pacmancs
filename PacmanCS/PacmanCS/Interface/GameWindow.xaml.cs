﻿using PacmanCS.Enumerable;
using SharpGL;
using SharpGL.SceneGraph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PacmanCS.Interface
{
    /// <summary>
    /// Interaction logic for GameWindow.xaml
    /// </summary>
    public partial class GameWindow : Window
    {
        public GameKey GameKey { get; set; }
        private static GameKey[] keys = (GameKey[])Enum.GetValues(typeof(GameKey));
        public ControlsWindow ControlsWindow { get; set; }
        public GameWindow()
        {
            InitializeComponent();
            EventManager.RegisterClassHandler(typeof(Window), Keyboard.KeyUpEvent, new KeyEventHandler(KeyReleased), true);
            EventManager.RegisterClassHandler(typeof(Window), Keyboard.KeyDownEvent, new KeyEventHandler(KeyPressed), true);
            ControlsWindow = new ControlsWindow();
        }

        private void KeyReleased(object sender, KeyEventArgs e)
        {
            GameKey key = GameKey.None;
            if (e.Key == ControlsWindow.North)
            {
                key = GameKey.North;
            }
            else if (e.Key == ControlsWindow.East)
            {
                key = GameKey.East;
            }
            else if (e.Key == ControlsWindow.South)
            {
                key = GameKey.South;
            }
            else if (e.Key == ControlsWindow.West)
            {
                key = GameKey.West;
            }
            else if (e.Key == ControlsWindow.Confirm)
            {
                key = GameKey.Confirm;
            }
            else if (e.Key == ControlsWindow.Cancel)
            {
                key = GameKey.Cancel;
            }
            else if (e.Key == ControlsWindow.Camera)
            {
                key = GameKey.Camera;
            }
            else if (e.Key == ControlsWindow.Pause)
            {
                key = GameKey.Pause;
            }
            GameKey ^= key;
            Console.WriteLine(GameKey);
        }
        private void KeyPressed(object sender, KeyEventArgs e)
        {
            GameKey = GameKey.None;
            if (e.Key == ControlsWindow.North)
            {
                GameKey |= GameKey.North;
            }
            else if(e.Key == ControlsWindow.East)
            {
                GameKey |= GameKey.East;
            }
            else if(e.Key == ControlsWindow.South)
            {
                GameKey |= GameKey.South;
            }
            else if(e.Key == ControlsWindow.West)
            {
                GameKey |= GameKey.West;
            }
            else if(e.Key == ControlsWindow.Confirm)
            {
                GameKey |= GameKey.Confirm;
            }
            else if(e.Key == ControlsWindow.Cancel)
            {
                GameKey |= GameKey.Cancel;
            }
            else if(e.Key == ControlsWindow.Camera)
            {
                GameKey |= GameKey.Camera;
            }
            else if(e.Key == ControlsWindow.Pause)
            {
                GameKey |= GameKey.Pause;
            }
            Console.WriteLine(GameKey);
        }

        private void OpenGLControl_OpenGLInitialized(object sender, OpenGLEventArgs args)
        {
            //  Get the OpenGL object.
            OpenGL gl = openGLControl.OpenGL;

            //  Set the clear color.
            gl.ClearColor(0, 0, 0, 0);
        }
        private void OpenGLControl_Resized(object sender, OpenGLEventArgs args)
        {
            //  Get the OpenGL object.
            OpenGL gl = openGLControl.OpenGL;

            //  Set the projection matrix.
            gl.MatrixMode(OpenGL.GL_PROJECTION);

            //  Load the identity.
            gl.LoadIdentity();

            //  Create a perspective transformation.
            gl.Perspective(60.0f, Width / Height, 0.01, 100.0);

            //  Use the 'look at' helper function to position and aim the camera.
            gl.LookAt(-5, 5, -5, 0, 0, 0, 0, 1, 0);

            //  Set the modelview matrix.
            gl.MatrixMode(OpenGL.GL_MODELVIEW);
        }
        private void OpenGLControl_OpenGLDraw(object sender, OpenGLEventArgs args)
        {
            //  Get the OpenGL object.
            OpenGL gl = openGLControl.OpenGL;

            //  Clear the color and depth buffer.
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);


            //  Draw a coloured pyramid.
            gl.Begin(OpenGL.GL_TRIANGLES);
            gl.Color(1.0f, 0.0f, 0.0f);
            gl.Vertex(0.0f, 2.0f, 0.0f);
            gl.Vertex(0.0f, 0.0f, 2.0f);
            gl.Vertex(2.0f, 0.0f, 0.0f);
            gl.End();
        }

        private void Settings_Click(object sender, RoutedEventArgs e)
        {
            ControlsWindow.ShowDialog();
        }
    }
}

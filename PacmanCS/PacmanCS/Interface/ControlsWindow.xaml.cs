﻿using PacmanCS.Enumerable;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PacmanCS.Interface
{
    /// <summary>
    /// Interaction logic for Controls.xaml
    /// </summary>
    public partial class ControlsWindow : Window, INotifyPropertyChanged
    {
        private Key _north;
        private Key _east;
        private Key _south;
        private Key _west;
        private Key _confirm;
        private Key _cancel;
        private Key _camera;
        private Key _pause;
        private List<Key> _availableKeys = GetAvailableKeys();

        public Key North { get => _north; set { _north = value; NotifyPropertyChanged(); } }
        public Key East { get => _east; set { _east = value; NotifyPropertyChanged(); } }
        public Key South { get => _south; set { _south = value; NotifyPropertyChanged(); } }
        public Key West { get => _west; set { _west = value; NotifyPropertyChanged(); } }
        public Key Confirm { get => _confirm; set { _confirm = value; NotifyPropertyChanged(); } }
        public Key Cancel { get => _cancel; set { _cancel = value; NotifyPropertyChanged(); } }
        public Key Camera { get => _camera; set { _camera = value; NotifyPropertyChanged(); } }
        public Key Pause { get => _pause; set { _pause = value; NotifyPropertyChanged(); } }

        public List<Key> AvailableKeys { get => _availableKeys; }
        public Dictionary<Key, GameKey> ChosenWindowsKeys = new Dictionary<Key, GameKey>();
        public Dictionary<GameKey, Key> ChosenControlKeys = new Dictionary<GameKey, Key>();

        public event PropertyChangedEventHandler PropertyChanged;

        public ControlsWindow()
        {
            InitializeComponent();
            Deserialize();
            ChosenControlKeys = new Dictionary<GameKey, Key> {
                { GameKey.North, North },
                { GameKey.East, East },
                { GameKey.South, South },
                { GameKey.West, West },
                { GameKey.Confirm, Confirm },
                { GameKey.Cancel, Cancel },
                { GameKey.Camera, Camera },
                { GameKey.Pause, Pause }
            };
            ChosenWindowsKeys = ChosenControlKeys.ToDictionary(k => k.Value, k => k.Key);
            //AvailableKeys.RemoveAll(k => ChosenWindowsKeys.Select(f => f.Key).ToList().Contains(k));
            DataContext = this;
        }

        public static List<Key> GetAvailableKeys()
        {
            List<Key> list = new List<Key>();
            list.AddRange(((Key[])Enum.GetValues(typeof(Key))).Where(k => (int)k > 43 && (int)k < 70));
            list.Add(Key.Up);
            list.Add(Key.Right);
            list.Add(Key.Down);
            list.Add(Key.Left);
            list.Add(Key.Escape);
            list.Add(Key.Enter);
            return list;
        }

        public void Deserialize()
        {
            North = Key.W;
            East = Key.D;
            South = Key.S;
            West = Key.A;
            Confirm = Key.Enter;
            Cancel = Key.Escape;
            Camera = Key.C;
            Pause = Key.P;
        }

        protected void NotifyPropertyChanged([CallerMemberName]string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }
    }
}

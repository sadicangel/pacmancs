﻿using PacmanCS.Enumerable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace PacmanCS.Base
{
    public abstract class Unit : IRenderable
    {
        protected static Random Random = new Random(DateTime.Now.Millisecond);
        public Vector3 Position { get; set; }
        public UnitType UnitType { get; set; }
        public abstract void Render();
    }
}

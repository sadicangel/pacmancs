﻿using PacmanCS.Moving;
using PacmanCS.Static;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacmanCS.Base
{
    public interface IPickUp
    {
        void PickUp(Pacman pacman);
    }
    
    public class PickUpScore : IPickUp
    {
        public void PickUp(Pacman pacman)
        {
            pacman.Score += pacman.HasPowerUp(Enumerable.UnitPowerUp.Twice) ? Ball.Value * 2 : pacman.HasPowerUp(Enumerable.UnitPowerUp.Half) ? Ball.Value / 2 : Ball.Value;
        }
    }

    public class PickUpFruit : IPickUp
    {
        public void PickUp(Pacman pacman)
        {
            pacman.Score += pacman.HasPowerUp(Enumerable.UnitPowerUp.Twice) ? Fruit.Value * 2 : pacman.HasPowerUp(Enumerable.UnitPowerUp.Half) ? Fruit.Value / 2 : Fruit.Value;
        }
    }

    public class PickUpTeleport : IPickUp
    {
        public void PickUp(Pacman pacman)
        {
            throw new NotImplementedException();
        }
    }

    public class PickUpHunter : IPickUp
    {
        public void PickUp(Pacman pacman)
        {
            pacman.Score += pacman.HasPowerUp(Enumerable.UnitPowerUp.Twice) ? Ghost.BountyValue * 2 : pacman.HasPowerUp(Enumerable.UnitPowerUp.Half) ? Ghost.BountyValue / 2 : Ghost.BountyValue;
        }
    }

    public class PickUpHaste : IPickUp
    {
        public void PickUp(Pacman pacman)
        {
            pacman.PowerUp |= Enumerable.UnitPowerUp.Haste;
        }
    }
    public class PickUpSlow : IPickUp
    {
        public void PickUp(Pacman pacman)
        {
            pacman.PowerUp |= Enumerable.UnitPowerUp.Slow;
        }
    }
    public class PickUpTwice : IPickUp
    {
        public void PickUp(Pacman pacman)
        {
            pacman.PowerUp |= Enumerable.UnitPowerUp.Twice;
        }
    }
    public class PickUpHalf : IPickUp
    {
        public void PickUp(Pacman pacman)
        {
            pacman.PowerUp |= Enumerable.UnitPowerUp.Half;
        }
    }
    public class PickUpBrutality : IPickUp
    {
        public void PickUp(Pacman pacman)
        {
            pacman.PowerUp |= Enumerable.UnitPowerUp.Brutality;
        }
    }
    public class PickUpPacifism : IPickUp
    {
        public void PickUp(Pacman pacman)
        {
            pacman.PowerUp |= Enumerable.UnitPowerUp.Pacifism;
        }
    }
}

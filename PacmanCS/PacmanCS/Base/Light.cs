﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace PacmanCS.Base
{
    public abstract class Light : Unit
    {
        private const Int32 AvailableLights = 8;
        private static Boolean[] Lights = new Boolean[AvailableLights];

        public Int32 ID { get; set; }
        public Boolean IsEnabled { get; set; }

        public static int GetLightID()
        {
            for (int i = 0; i < AvailableLights; ++i)
            {
                if (!Lights[i])
                {
                    Lights[i] = true;
                    return i;
                }
            }
            throw new Exception("Exceeded maximum number of lights.");
        }

        public void Toggle()
        {
            IsEnabled = !IsEnabled;
        }

        public abstract void Change(Vector3 position, Vector3 direction);
    }
}

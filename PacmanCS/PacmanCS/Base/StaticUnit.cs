﻿using PacmanCS.Moving;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacmanCS.Base
{
    public abstract class StaticUnit : Unit
    {
        public Boolean IsPassable { get; set; }
        public IPickUp PickUp { get; set; }

        public void ConsumePickUp(Pacman pacman)
        {
            PickUp.PickUp(pacman);
        }
    }
}

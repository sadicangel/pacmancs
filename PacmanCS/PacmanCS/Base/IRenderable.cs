﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacmanCS.Base
{
    public interface IRenderable
    {
        void Render();
    }
}

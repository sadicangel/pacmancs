﻿using PacmanCS.Enumerable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PacmanCS.Base
{
    public abstract class MovingUnit : Unit
    {
        public const Single BaseSpeed = 5f;
        public const Single SpeedAdd = 1.05f;
        public const Single SpeedDiv = 1000f;
        public static readonly Vector3 North = new Vector3(0, 0, -1);
        public static readonly Vector3 East = new Vector3(-1, 0, 0);
        public static readonly Vector3 South = new Vector3(0, 0, 1);
        public static readonly Vector3 West = new Vector3(1, 0, 0);

        public Vector3 StartPosition { get; set; }
        public UnitState UnitState { get; set; }
        public UnitAction UnitAction { get; set; }
        public Vector3 UnitDirection { get; set; }
        public Vector3 NextDirection { get; set; }
        public Int32 Speed { get; set; }

        public abstract void Update(float time);
        public abstract void Turn(Key key, bool up);
    }
}

﻿using PacmanCS.Enumerable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace PacmanCS.Base
{
    public interface IGhostType
    {
        Vector3 ChosePosition(Vector3 pacmanPosition, Vector3 pacmanDirection);
    }

    public class PursuerGhost : IGhostType
    {
        public Vector3 ChosePosition(Vector3 pacmanPosition, Vector3 pacmanDirection)
        {
            return pacmanPosition;
        }
    }

    public class AmbusherGhost : IGhostType
    {
        public Vector3 ChosePosition(Vector3 pacmanPosition, Vector3 pacmanDirection)
        {
            return pacmanDirection * 4 + pacmanPosition;
        }
    }

    public class WhimsicalGhost : IGhostType
    {
        public Vector3 ChosePosition(Vector3 pacmanPosition, Vector3 pacmanDirection)
        {
            throw new NotImplementedException();
        }
    }

    public class IgnorantGhost : IGhostType
    {
        public Vector3 ChosePosition(Vector3 pacmanPosition, Vector3 pacmanDirection)
        {
            if (Vector3.DistanceSquared(pacmanPosition, pacmanPosition) > 64)
                return pacmanPosition;
            return pacmanPosition;
        }
    }
}

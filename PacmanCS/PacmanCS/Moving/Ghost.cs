﻿using PacmanCS.Base;
using PacmanCS.Enumerable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PacmanCS.Moving

{
    public class Ghost : MovingUnit
    {
        public const Single Radius = 0.3f;
        public const Int32 BountyValue = 10;
        public const Int32 RespawnTime = 5000;
        public const Single RespawnSpeed = BaseSpeed / 2f;
        public static readonly List<Vector3> Colors = new List<Vector3>() {
            new Vector3(0.7f, 0.0f, 0.0f), // red
            new Vector3(0.7f, 0.0f, 0.7f), // pink
            new Vector3(0.0f, 0.7f, 0.0f), // green
            new Vector3(0.0f, 0.2f, 0.9f)  // blue
        };
        public static readonly Vector3 FleeColor = new Vector3(0.6f, 0.6f, 0.6f);

        public static Vector3 PacmanPosition { get; set; }
        public static Vector3 PacmanDirection { get; set; }

        public Int32 ID { get; set; }
        public IGhostType GhostType { get; set; }
        public Single SpawnTime { get; set; }
        public Single Difficulty { get; set; }

        private Ghost(IGhostType type, Vector3 position, int id, float difficulty, float spawnTime)
        {
            UnitType = UnitType.Ghost;
            UnitState = UnitState.Alive;
            UnitAction = UnitAction.Chase;
            UnitDirection = South;
            NextDirection = South;
            StartPosition = position;
            Position = position;
            ID = id;
            GhostType = type;
            Difficulty = difficulty;
            SpawnTime = spawnTime;
        }

        public override void Render()
        {
            throw new NotImplementedException();
        }

        public override void Turn(Key key, bool up)
        {
            Vector3 desired = GhostType.ChosePosition(PacmanPosition, PacmanDirection);
        }

        public override void Update(float time)
        {
            throw new NotImplementedException();
        }
    }
}

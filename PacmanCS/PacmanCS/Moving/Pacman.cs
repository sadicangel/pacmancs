﻿using PacmanCS.Base;
using PacmanCS.Enumerable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PacmanCS.Moving
{
    public class Pacman : MovingUnit
    {
        public const Single Radius = 0.35f;
        public const Int32 TotalRespawnTime = 1000;
        public const Single ExplodeTime = TotalRespawnTime / 2f;
        public const Single InvisibleTime = TotalRespawnTime / 4f;
        public const Single BaseBlinkTime = 0f;
        public const Single BlinkDuration = TotalRespawnTime / 40f;

        public Int32 Lives { get; set; }
        public Int32 Score { get; set; }
        public Single RespawnTime { get; set; }
        public Single BlinkTime { get; set; }
        public UnitPowerUp PowerUp { get; set; }

        public Pacman(Vector3 position)
        {
            UnitType = UnitType.Pacman;
            UnitState = UnitState.Alive;
            UnitAction = UnitAction.Flee;
            UnitDirection = South;
            NextDirection = South;
            StartPosition = position;
            Position = position;
            PowerUp = UnitPowerUp.None;
            Lives = 3;
        }

        public bool HasPowerUp(UnitPowerUp powerUp)
        {
            return (PowerUp & powerUp) == PowerUp;
        }

        public override void Render()
        {
            throw new NotImplementedException();
        }

        public override void Turn(Key key, bool up)
        {
            throw new NotImplementedException();
        }

        public override void Update(float time)
        {
            throw new NotImplementedException();
        }
    }
}

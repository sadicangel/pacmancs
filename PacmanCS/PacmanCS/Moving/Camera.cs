﻿using PacmanCS.Base;
using PacmanCS.Enumerable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PacmanCS.Moving
{
    public class Camera : MovingUnit
    {
        public Vector3 UnitPosition { get; set; }
        public Vector3 UnitOffset { get; set; }
        public Vector3 ViewPosition { get; set; }
        public Vector3 ViewOffset { get; set; }
        public Single AspectRatio { get; set; }
        public CameraView View { get; set; }

        public override void Render()
        {
            switch (View)
            {
                case CameraView.FollowView:
                    goto default;
                case CameraView.RotateView:
                    goto default;
                case CameraView.TopView:
                    goto default;
                default:
                    throw new NotImplementedException();
            }
        }

        public override void Turn(Key key, bool up)
        {
            throw new NotImplementedException();
        }

        public override void Update(float aspectRatio)
        {
            throw new NotImplementedException();
        }
    }
}

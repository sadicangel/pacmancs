﻿using PacmanCS.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace PacmanCS.Moving
{
    public class SpotLight : Light
    {
        public Vector3 Direction { get; set; }

        public SpotLight(Vector3 position, Vector3 direction, bool enabled = true)
        {
            Position = position;
            Direction = direction;
            UnitType = Enumerable.UnitType.Light;
            ID = GetLightID();
            IsEnabled = enabled;
        }

        public override void Change(Vector3 position, Vector3 direction)
        {
            throw new NotImplementedException();
        }

        public override void Render()
        {
            if (IsEnabled)
            {
                throw new NotImplementedException();
            }
        }
    }
}

﻿using PacmanCS.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacmanCS.Controller
{
    public interface IGameState
    {
        void Render(Game game);
    }

    public class PausedState : IGameState
    {
        public void Render(Game game)
        {
            throw new NotImplementedException();
        }
    }

    public class RunningState : IGameState
    {
        public void Render(Game game)
        {
            throw new NotImplementedException();
        }
    }

    public class OnMenuState : IGameState
    {
        public void Render(Game game)
        {
            throw new NotImplementedException();
        }
    }

    public class CountDownState : IGameState
    {
        public void Render(Game game)
        {
            throw new NotImplementedException();
        }
    }

    public class WinningState : IGameState
    {
        public void Render(Game game)
        {
            throw new NotImplementedException();
        }
    }

    public class LosingState : IGameState
    {
        public void Render(Game game)
        {
            throw new NotImplementedException();
        }
    }
}

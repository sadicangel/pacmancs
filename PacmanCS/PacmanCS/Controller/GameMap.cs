﻿using PacmanCS.Base;
using PacmanCS.Moving;
using PacmanCS.Static;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace PacmanCS.Controller
{
    public class GameMap : IRenderable
    {
        public const Int32 MaxDifficulty = 5;
        public const Int32 MinDifficulty = 1;

        public String MapFile { get; set; }
        public Int32 Difficulty { get; set; }
        public Int32 Rows { get; set; }
        public Int32 Columns { get; set; }
        public Int32 Balls { get; set; }
        public Int32 Hunters { get; set; }
        public Int32 Crates { get; set; }
        public Int32 Fruits { get; set; }
        public Int32 Ghosts { get; set; }
        public List<Vector3> GhostPositions { get; set; }
        public Vector3 PacmanPosition { get; set; }
        public Vector3 DoorPosition { get; set; }
        public Floor Floor { get; set; }
        private StaticUnit[,] Map { get; set; }

        public GameMap(string file)
        {
            try
            {
                using (StreamReader fs = new StreamReader(file))
                {
                    Difficulty = Int32.Parse(fs.ReadLine());
                    Rows = Int32.Parse(fs.ReadLine());
                    Columns = Int32.Parse(fs.ReadLine());
                    Map = new StaticUnit[Rows, Columns];
                    GhostPositions = new List<Vector3>();
                    Floor = new Floor(new Vector3(0, 0, 0), Rows, Columns);
                    for (int z = 0; z < Rows; ++z)
                    {
                        string[] values = fs.ReadLine().Split(' ');
                        if (Columns != values.Length) throw new Exception("Line " + z + " number of columns is incorrect.");
                        for (int x = 0; x < Columns; ++x)
                        {
                            Vector3 position = new Vector3(x, 0, z);
                            switch (Int32.Parse(values[x]))
                            {
                                case 1: // Wall
                                    Map[z, x] = new Wall(position); break;
                                case 2: // Ball
                                    Map[z, x] = new Ball(position); ++Balls; break;
                                case 3: // Hunter
                                    Map[z, x] = new Hunter(position); ++Hunters; break;
                                case 4: // Crate
                                    Map[z, x] = new Crate(position); ++Crates; break;
                                case 5: // Fruit
                                    Map[z, x] = new Fruit(position); ++Fruits; break;
                                case 6: // Teleport
                                    Map[z, x] = new Teleport(position, position); break;
                                case 7: // Door
                                    Map[z, x] = new Door(position); DoorPosition = position; break;
                                case 8: // Ghost
                                    GhostPositions.Add(position); Ghosts++; break;
                                case 9: // Pacman
                                    PacmanPosition = position; break;
                                default: break;
                            }
                        }
                    }
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public bool IsPassable(Vector3 position)
        {
            return Map[(int)position.Z, (int)position.X].IsPassable;
        }

        public void Assign(Vector3 position, StaticUnit unit)
        {
            Map[(int)position.Z, (int)position.X] = unit;
        }

        public void Remove(Vector3 position)
        {
            Map[(int)position.Z, (int)position.X] = null;
        }

        public void Collect(Vector3 position, Pacman pacman)
        {
            Map[(int)position.Z, (int)position.X]?.PickUp?.PickUp(pacman);
        }

        public void Render()
        {
            Floor.Render();
            foreach(StaticUnit unit in Map)
                unit?.Render();
        }
    }
}

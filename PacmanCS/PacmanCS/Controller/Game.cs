﻿using PacmanCS.Base;
using PacmanCS.Enumerable;
using PacmanCS.Moving;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacmanCS.Controller
{
    public class Game : IRenderable
    {
        public const Single CountdownTime = 4000f;
        public const Int32 RefreshTime = 30;
        public const Int32 SpawnInterval = 1000;
        public const Int32 BonusDuration = 5000;
        
        public Int32 Volume { get; set; }
        public Single DayTime { get; set; }
        public Single Countdown { get; set; }

        public IGameState PreviousState { get; set; }
        public IGameState GameState { get; set; }
        public Int64 LastTick { get; set; }
        public Int32 Score { get; set; }
        public Int32 Balls { get; set; }

        public GameMap Map { get; set; }
        // window & camera
        public Pacman Pacman { get; set; }
        public List<Ghost> Ghosts { get; set; }
        public List<Light> Lights { get; set; }
        // hud

        public void Render()
        {
            GameState.Render(this);
        }
    }
}
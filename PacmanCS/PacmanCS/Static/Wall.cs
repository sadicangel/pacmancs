﻿using PacmanCS.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace PacmanCS.Static
{
    public class Wall : StaticUnit
    {
        public const Single Size = 0.5f;

        public Wall(Vector3 position)
        {
            Position = position;
            UnitType = Enumerable.UnitType.Wall;
            IsPassable = false;
            PickUp = null;
        }

        public override void Render()
        {
            throw new NotImplementedException();
        }
    }
}

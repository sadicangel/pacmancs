﻿using PacmanCS.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace PacmanCS.Static
{
    public class EmptySpace : StaticUnit
    {
        public EmptySpace(Vector3 position)
        {
            Position = position;
            UnitType = Enumerable.UnitType.Floor;
            IsPassable = true;
            PickUp = null;
        }

        public override void Render()
        {
            throw new NotImplementedException();
        }
    }
}

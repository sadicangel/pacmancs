﻿using PacmanCS.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace PacmanCS.Static
{
    public class Teleport : StaticUnit
    {
        public Vector3 Destination { get; set; }

        public Teleport(Vector3 position, Vector3 destination)
        {
            Position = position;
            Destination = destination;
            UnitType = Enumerable.UnitType.Teleport;
            PickUp = new PickUpTeleport();
        }

        public override void Render()
        {
            throw new NotImplementedException();
        }
    }
}

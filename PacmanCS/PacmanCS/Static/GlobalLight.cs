﻿using PacmanCS.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace PacmanCS.Static
{
    public class GlobalLight : Light
    {
        public GlobalLight(Vector3 position, bool enabled = true)
        {
            Position = position;
            UnitType = Enumerable.UnitType.Light;
            ID = GetLightID();
            IsEnabled = enabled;
        }

        public override void Change(Vector3 position, Vector3 direction)
        {
            // Does not change
        }

        public override void Render()
        {
            if (IsEnabled)
            {
                throw new NotImplementedException();
            }
        }
    }
}

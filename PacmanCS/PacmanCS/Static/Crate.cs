﻿using PacmanCS.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace PacmanCS.Static
{
    public class Crate : StaticUnit
    {
        public const Single Side = 0.30f;
        
        public Single Rotation { get; set; }

        public Crate(Vector3 position)
        {
            Position = position;
            UnitType = Enumerable.UnitType.Pickup;
            PickUp = CreateRandomPickUp();
            IsPassable = true;
            Rotation = (float)Random.NextDouble() * 360f;
        }

        public static IPickUp CreateRandomPickUp()
        {
            switch (Random.Next(6))
            {
                case 0: return new PickUpHaste();
                case 1: return new PickUpSlow();
                case 2: return new PickUpTwice();
                case 3: return new PickUpHalf();
                case 4: return new PickUpBrutality();
                case 5: return new PickUpPacifism();
            }
            return null;
        }

        public override void Render()
        {
            throw new NotImplementedException();
        }
    }
}

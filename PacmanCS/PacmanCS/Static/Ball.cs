﻿using PacmanCS.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace PacmanCS.Static
{
    public class Ball : StaticUnit
    {
        public const Int32 Value = 2;
        public const Single Radius = 0.125f;
        public const Single MaxBounce = 0.1f;
        public const Single BouncePerTick = 0.005f;

        public Single BounceRange { get; set; }
        public Boolean IsRising { get; set; }
        
        public Ball(Vector3 position)
        {
            Position = position;
            UnitType = Enumerable.UnitType.Pickup;
            PickUp = new PickUpScore();
            IsPassable = true;
            IsRising = true;
            BounceRange = (float)Random.NextDouble() * MaxBounce;
        }

        public override void Render()
        {
            throw new NotImplementedException();
        }
    }
}

﻿using PacmanCS.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace PacmanCS.Static
{
    public class Door : StaticUnit
    {
        public Door(Vector3 position)
        {
            Position = position;
            UnitType = Enumerable.UnitType.Wall;
            IsPassable = false;
            PickUp = null;
        }

        public override void Render()
        {
            throw new NotImplementedException();
        }
    }
}

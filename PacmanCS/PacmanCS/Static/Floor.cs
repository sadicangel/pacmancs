﻿using PacmanCS.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace PacmanCS.Static
{
    public class Floor : Unit
    {
        public const Single TILE_SIZE = 1f;
        public Int32 Rows { get; set; }
        public Int32 Columns { get; set; }

        public Floor(Vector3 position, int rows, int columns)
        {
            Position = position;
            UnitType = Enumerable.UnitType.Floor;
            Rows = rows;
            Columns = columns;
        }

        public Floor(Vector3 position, int size) : this(position, size, size) { }

        public override void Render()
        {
            throw new NotImplementedException();
        }
    }
}

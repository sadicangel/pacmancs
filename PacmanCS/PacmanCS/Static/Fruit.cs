﻿using PacmanCS.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace PacmanCS.Static
{
    public class Fruit : StaticUnit
    {
        public const Int32 Value = 20;
        public const Single Size = 0.3f;
        
        public Fruit(Vector3 position)
        {
            Position = position;
            UnitType = Enumerable.UnitType.Pickup;
            IsPassable = true;
            PickUp = new PickUpFruit();
        }

        public override void Render()
        {
            throw new NotImplementedException();
        }
    }
}

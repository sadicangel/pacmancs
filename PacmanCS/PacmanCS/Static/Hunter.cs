﻿using PacmanCS.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace PacmanCS.Static
{
    public class Hunter : StaticUnit
    {
        public const Single Radius = 0.25f;

        public Hunter(Vector3 position)
        {
            Position = position;
            UnitType = Enumerable.UnitType.Pickup;
            IsPassable = true;
            PickUp = new PickUpHunter();
        }
        public override void Render()
        {
            throw new NotImplementedException();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacmanCS.Enumerable
{
    public enum UnitType
    {
        Menu, Camera, Light, Floor, Wall, Teleport, Particle, Pickup, Ghost, Pacman
    }
}

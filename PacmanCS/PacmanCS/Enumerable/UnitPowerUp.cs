﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacmanCS.Enumerable
{
    [Flags]
    public enum UnitPowerUp
    {
        None = 0,
        Hunter = 1,
        Haste = 2,
        Slow = 4,
        Twice = 8,
        Half = 16,
        Pacifism = 32,
        Brutality = 64,
        All = 255
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacmanCS.Enumerable
{
    public enum UnitAction
    {
        Chase, Flee, Scatter
    }
}

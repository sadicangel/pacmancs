﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacmanCS.Enumerable
{
    public enum CameraView
    {
        TopView, FollowView, RotateView
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacmanCS.Enumerable
{
    [Flags]
    public enum UnitState
    {
        Alive = 1,
        Dead = 2,
        Respawning = 4,
        Crossing = 8,
        Blinking = 16,
        Waiting = 32
    }
}

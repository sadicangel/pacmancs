﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacmanCS.Enumerable
{
    public enum UnitDirection
    {
        North = 0,
        East = 90,
        South = 180,
        West = -90
    }
}

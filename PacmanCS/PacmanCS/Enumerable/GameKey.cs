﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacmanCS.Enumerable
{
    [Flags]
    public enum GameKey
    {
        None = 0,
        North = 1,
        East = 2,
        South = 4,
        West = 8,
        Confirm = 16,
        Cancel = 32,
        Camera = 64,
        Pause = 128,
        All = 255
    }
}
